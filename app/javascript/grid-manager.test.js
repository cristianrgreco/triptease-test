import { Block, COLOURS } from './grid';
import { removeBlockFromGrid, removeBlockFromDocument } from './grid-manager';
import { assert } from 'chai';

describe('removeBlockFromDocument', () => {

  it('should move the block element to the end of the column and update the element IDs', () => {
    document.body.innerHTML = `
      <div id="col_0">
        <div id="block_0x2">1</div>
        <div id="block_0x1">2</div>
        <div id="block_0x0">3</div>
      </div>
    `;

    const block = new Block(0, 0, COLOURS[0]);

    removeBlockFromDocument(block, 3);

    const columnElement = document.getElementById('col_0');
    assert.equal(columnElement.childNodes[0].textContent, '3');
    assert.equal(columnElement.childNodes[0].style.visibility, 'hidden');

    assert.equal(columnElement.childNodes[0].id, 'block_0x2');
    assert.equal(columnElement.childNodes[1].id, 'block_0x1');
    assert.equal(columnElement.childNodes[2].id, 'block_0x0');
  });
});

describe('removeBlockFromGrid', () => {

  it('should move the block to the end of the column and update the Y coordinates', () => {
    const block = new Block(0, 0, COLOURS[0]);
    const grid = [
      [
        new Block(0, 0, COLOURS[0]),
        new Block(0, 1, COLOURS[1]),
        new Block(0, 2, COLOURS[2])
      ]
    ];

    removeBlockFromGrid(block, grid, 3);

    assert.deepEqual(grid, [
      [
        new Block(0, 0, COLOURS[1]),
        new Block(0, 1, COLOURS[2]),
        new Block(0, 2, null)
      ]
    ]);
  });
});
