export function removeBlock(block, grid, maxHeight) {
  removeBlockFromDocument(block, maxHeight);
  removeBlockFromGrid(block, grid, maxHeight);
}

export function removeBlockFromDocument(block, maxHeight) {
  const columnElement = document.getElementById(columnElementIdentifier(block.x));
  const blockElement = columnElement.removeChild(document.getElementById(blockElementIdentifier(block.x, block.y)));

  blockElement.style.visibility = 'hidden';
  columnElement.insertBefore(blockElement, columnElement.childNodes[0]);

  for (let i = maxHeight - 1; i >= 0; i--) {
    const blockElementAtIndex = columnElement.childNodes[(maxHeight - 1) - i];
    blockElementAtIndex.id = blockElementIdentifier(block.x, i);
  }
}

export function removeBlockFromGrid(block, grid, maxHeight) {
  block.colour = null;

  grid[block.x] = [
    ...grid[block.x].slice(0, block.y),
    ...grid[block.x].slice(block.y + 1, maxHeight),
    block
  ];

  for (let blockIndex = 0; blockIndex < maxHeight; blockIndex++) {
    grid[block.x][blockIndex].y = blockIndex;
  }
}

function columnElementIdentifier(x) {
  return `col_${x}`;
}

function blockElementIdentifier(x, y) {
  return `block_${x}x${y}`;
}
