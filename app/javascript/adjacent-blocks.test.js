import { Block, COLOURS } from './grid';
import { getAdjacentBlocks } from './adjacent-blocks';
import { assert } from 'chai';

describe('getAdjacentBlocks', () => {

  it('should return the starting block if there are no adjacent blocks of the same colour', () => {
    const startBlock = new Block(0, 0, COLOURS[0]);
    const grid = [
      [startBlock]
    ];

    const adjacentBlocks = getAdjacentBlocks(startBlock, grid, 1, 1);

    assert.deepEqual(adjacentBlocks, [startBlock]);
  });

  it('should return all blocks of the same colour adjacent to the starting block', () => {
    const startBlock = new Block(1, 1, COLOURS[0]);
    const grid = [
      [new Block(0, 0, COLOURS[0]), new Block(1, 0, COLOURS[0]), new Block(2, 0, COLOURS[1])],
      [new Block(0, 1, COLOURS[0]), startBlock, new Block(2, 1, COLOURS[0])],
      [new Block(0, 2, COLOURS[0]), new Block(1, 2, COLOURS[1]), new Block(2, 2, COLOURS[1])]
    ];

    const adjacentBlocks = getAdjacentBlocks(startBlock, grid, 3, 3);

    assert.sameDeepMembers(adjacentBlocks, [
      new Block(1, 1, COLOURS[0]),
      new Block(2, 1, COLOURS[0]),
      new Block(0, 1, COLOURS[0]),
      new Block(0, 0, COLOURS[0]),
      new Block(0, 2, COLOURS[0]),
      new Block(1, 0, COLOURS[0])
    ]);
  });
});