import { getAdjacentBlocks } from './adjacent-blocks';
import { removeBlock } from './grid-manager';

export const COLOURS = ['red', 'green', 'blue', 'yellow'];
const MAX_X = 10;
const MAX_Y = 10;

export class Block {
  constructor(x, y, colour = COLOURS[Math.floor(Math.random() * COLOURS.length)]) {
    this.x = x;
    this.y = y;
    this.colour = colour;
  }
}

export class BlockGrid {
  constructor() {
    this.grid = [];

    for (let x = 0; x < MAX_X; x++) {
      let col = [];
      for (let y = 0; y < MAX_Y; y++) {
        col.push(new Block(x, y));
      }

      this.grid.push(col);
    }

    return this;
  }

  render(el = document.querySelector('#gridEl')) {
    for (let x = 0; x < MAX_X; x++) {
      let id = 'col_' + x;
      let colEl = document.createElement('div');
      colEl.className = 'col';
      colEl.id = id;
      el.appendChild(colEl);

      for (let y = MAX_Y - 1; y >= 0; y--) {
        let block = this.grid[x][y],
          id = `block_${x}x${y}`,
          blockEl = document.createElement('div');

        blockEl.id = id;
        blockEl.className = 'block';
        blockEl.style.background = block.colour;
        blockEl.addEventListener('click', evt => this.blockClicked(evt, block));
        colEl.appendChild(blockEl);
      }
    }

    return this;
  }

  blockClicked(e, block) {
    console.log(e, block);

    getAdjacentBlocks(block, this.grid, MAX_X, MAX_Y)
      .forEach(adjacentBlock => removeBlock(adjacentBlock, this.grid, MAX_Y))
  }
}

window.addEventListener('DOMContentLoaded', () => new BlockGrid().render());
