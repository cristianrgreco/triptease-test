export function getAdjacentBlocks(startBlock, grid, maxWidth, maxHeight) {

  function isCoordinateWithinBounds(x, y) {
    return (x >= 0 && x < maxWidth) && (y >= 0 && y < maxHeight);
  }

  const adjacentBlocks = [];
  const blockQueue = [startBlock];
  const visitedBlocks = new Set();

  while (blockQueue.length) {
    const block = blockQueue.shift();

    if (visitedBlocks.has(block)) {
      continue;
    } else {
      visitedBlocks.add(block);
    }

    adjacentBlocks.push(block);

    nextCoordinates(block.x, block.y)
      .filter(coordinate => isCoordinateWithinBounds(coordinate.x, coordinate.y))
      .map(coordinate => grid[coordinate.x][coordinate.y])
      .filter(block => block.colour === startBlock.colour)
      .forEach(block => blockQueue.push(block));
  }

  return adjacentBlocks;
}

function nextCoordinates(x, y) {
  return [
    {x: x - 1, y: y},
    {x: x + 1, y: y},
    {x: x, y: y - 1},
    {x: x, y: y + 1}
  ];
}
